# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2023-05-02 21:07+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"
"X-Qt-Contexts: true\n"

#: core/applicationversionsource.cpp:21
msgctxt "KUserFeedback::ApplicationVersionSource|"
msgid "The version of the application."
msgstr "A versión da aplicación."

#: core/applicationversionsource.cpp:36
msgctxt "KUserFeedback::ApplicationVersionSource|"
msgid "Application version"
msgstr "Versión da aplicación"

#: core/auditloguicontroller.cpp:140
msgctxt "KUserFeedback::AuditLogUiController|"
msgid "Basic System Information"
msgstr "Información básica do sistema"

#: core/auditloguicontroller.cpp:142
msgctxt "KUserFeedback::AuditLogUiController|"
msgid "Basic Usage Statistics"
msgstr "Estatísticas básicas de uso"

#: core/auditloguicontroller.cpp:144
msgctxt "KUserFeedback::AuditLogUiController|"
msgid "Detailed System Information"
msgstr "Información detallada do sistema"

#: core/auditloguicontroller.cpp:146
msgctxt "KUserFeedback::AuditLogUiController|"
msgid "Detailed Usage Statistics"
msgstr "Estatísticas detalladas de uso"

#: core/auditloguicontroller.cpp:156
#, qt-format
msgctxt "KUserFeedback::AuditLogUiController|"
msgid "Unable to open file %1: %2."
msgstr "Non é posíbel abrir o ficheiro «%1»: %2."

#: core/auditloguicontroller.cpp:196
#, qt-format
msgctxt "KUserFeedback::AuditLogUiController|"
msgid "Category: <i>%1</i><br/>"
msgstr "Categoría: <i>%1</i><br/>"

#: core/auditloguicontroller.cpp:197
#, qt-format
msgctxt "KUserFeedback::AuditLogUiController|"
msgid "Key: <i>%1</i><br/>"
msgstr "Chave: <i>%1</i><br/>"

#: core/auditloguicontroller.cpp:198
#, qt-format
msgctxt "KUserFeedback::AuditLogUiController|"
msgid "Submitted data: <tt>%1</tt><br/><br/>"
msgstr "Datos enviados: <tt>%1</tt><br/><br/>"

#: core/compilerinfosource.cpp:20
msgctxt "KUserFeedback::CompilerInfoSource|"
msgid "The compiler used to build this application."
msgstr "O compilador usado para construír a aplicación."

#: core/compilerinfosource.cpp:53
msgctxt "KUserFeedback::CompilerInfoSource|"
msgid "Compiler information"
msgstr "Información do compilador"

#: core/cpuinfosource.cpp:22
msgctxt "KUserFeedback::CpuInfoSource|"
msgid "The amount and type of CPUs in the system."
msgstr "A cantidade e o tipo de CPU no sistema."

#: core/cpuinfosource.cpp:35
msgctxt "KUserFeedback::CpuInfoSource|"
msgid "CPU information"
msgstr "Información da CPU"

#: core/feedbackconfiguicontroller.cpp:119
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Disabled"
msgstr "Desactivada"

#: core/feedbackconfiguicontroller.cpp:121
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Basic system information"
msgstr "Información básica do sistema"

#: core/feedbackconfiguicontroller.cpp:123
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Basic system information and usage statistics"
msgstr "Información básica do sistema e estatísticas de uso básicas"

#: core/feedbackconfiguicontroller.cpp:125
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Detailed system information and basic usage statistics"
msgstr "Información detallada do sistema e estatísticas de uso básicas"

#: core/feedbackconfiguicontroller.cpp:127
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Detailed system information and usage statistics"
msgstr "Información detallada do sistema e estatísticas de uso"

#: core/feedbackconfiguicontroller.cpp:139
#: core/feedbackconfiguicontroller.cpp:162
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Don't share anything"
msgstr "Non compartir nada"

#: core/feedbackconfiguicontroller.cpp:143
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Share basic system information such as the version of the application and "
"the operating system"
msgstr ""
"Compartir información básica do sistema como a versión da aplicación e do "
"sistema operativo."

#: core/feedbackconfiguicontroller.cpp:147
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Share basic system information and basic statistics on how often you use the "
"application"
msgstr ""
"Compartir información básica do sistema e estatísticas básicas sobre a "
"frecuencia coa que usa a aplicación."

#: core/feedbackconfiguicontroller.cpp:151
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Share basic statistics on how often you use the application, as well as more "
"detailed information about your system"
msgstr ""
"Compartir estatísticas básicas sobre a frecuencia coa que usa a aplicación, "
"así como información máis detallada sobre o sistema."

#: core/feedbackconfiguicontroller.cpp:155
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Share detailed system information and statistics on how often individual "
"features of the application are used."
msgstr ""
"Compartir información detallada do sistema e estatísticas sobre a frecuencia "
"coa que usa funcionalidades concretas da aplicación."

#: core/feedbackconfiguicontroller.cpp:166
#, qt-format
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Share basic system information such as the version of %1 and and the "
"operating system"
msgstr ""
"Compartir información básica do sistema como a versión de %1 e do sistema "
"operativo."

#: core/feedbackconfiguicontroller.cpp:170
#, qt-format
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Share basic system information and basic statistics on how often you use %1"
msgstr ""
"Compartir información básica do sistema e estatísticas básicas sobre a "
"frecuencia coa que usa  %1."

#: core/feedbackconfiguicontroller.cpp:174
#, qt-format
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Share basic statistics on how often you use %1, as well as more detailed "
"information about your system"
msgstr ""
"Compartir estatísticas básicas sobre a frecuencia coa que usa %1, así como "
"información máis detallada sobre o sistema."

#: core/feedbackconfiguicontroller.cpp:178
#, qt-format
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Share detailed system information and statistics on how often individual "
"features of %1 are used."
msgstr ""
"Compartir información detallada do sistema e estatísticas sobre a frecuencia "
"coa que usa funcionalidades concretas de %1."

#: core/feedbackconfiguicontroller.cpp:231
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Don't participate in usability surveys"
msgstr "Non participar nas enquisas de ergonomía"

#: core/feedbackconfiguicontroller.cpp:235
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Participate in surveys about the application not more than four times a year"
msgstr "Participar nas enquisas da aplicación non máis de catro veces ao ano."

#: core/feedbackconfiguicontroller.cpp:239
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Participate in surveys about the application whenever one is available (they "
"can be deferred or skipped)"
msgstr ""
"Participar sempre nas enquisas da aplicación (sempre pode pospoñelas ou "
"saltalas)."

#: core/feedbackconfiguicontroller.cpp:246
#, qt-format
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Don't participate in usability surveys about %1"
msgstr "Non participar nas enquisas de ergonomía de %1"

#: core/feedbackconfiguicontroller.cpp:250
#, qt-format
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid "Participate in surveys about %1 not more than four times a year"
msgstr "Participar nas enquisas de %1 non máis de catro veces ao ano."

#: core/feedbackconfiguicontroller.cpp:254
#, qt-format
msgctxt "KUserFeedback::FeedbackConfigUiController|"
msgid ""
"Participate in surveys about %1 whenever one is available (they can be "
"deferred or skipped)"
msgstr ""
"Participar sempre nas enquisas de %1 (sempre pode pospoñelas ou saltalas)."

#: core/localeinfosource.cpp:21
msgctxt "KUserFeedback::LocaleInfoSource|"
msgid "The current region and language settings."
msgstr "A configuración rexional actual."

#: core/localeinfosource.cpp:35
msgctxt "KUserFeedback::LocaleInfoSource|"
msgid "Locale information"
msgstr "Información da configuración rexional"

#: core/openglinfosource.cpp:28
msgctxt "KUserFeedback::OpenGLInfoSource|"
msgid "Information about type, version and vendor of the OpenGL stack."
msgstr "Información sobre o tipo, versión e fabricante da rima de OpenGL."

#: core/openglinfosource.cpp:98
msgctxt "KUserFeedback::OpenGLInfoSource|"
msgid "OpenGL information"
msgstr "Información de OpenGL"

#: core/platforminfosource.cpp:21
msgctxt "KUserFeedback::PlatformInfoSource|"
msgid "Type and version of the operating system."
msgstr "Tipo e versión do sistema operativo."

#: core/platforminfosource.cpp:58
msgctxt "KUserFeedback::PlatformInfoSource|"
msgid "Platform information"
msgstr "Información da plataforma"

#: core/qpainfosource.cpp:21
msgctxt "KUserFeedback::QPAInfoSource|"
msgid "The Qt platform abstraction plugin."
msgstr "O complemento de Qt de abstracción de plataforma."

#: core/qpainfosource.cpp:33
msgctxt "KUserFeedback::QPAInfoSource|"
msgid "QPA information"
msgstr "Información de QPA"

#: core/qtversionsource.cpp:21
msgctxt "KUserFeedback::QtVersionSource|"
msgid "The Qt version used by this application."
msgstr "A versión de Qt que usa a aplicación."

#: core/qtversionsource.cpp:33
msgctxt "KUserFeedback::QtVersionSource|"
msgid "Qt version information"
msgstr "Información da versión de Qt"

#: core/screeninfosource.cpp:22
msgctxt "KUserFeedback::ScreenInfoSource|"
msgid "Size and resolution of all connected screens."
msgstr "O tamaño e maila resolución de todas as pantallas conectadas."

#: core/screeninfosource.cpp:41
msgctxt "KUserFeedback::ScreenInfoSource|"
msgid "Screen parameters"
msgstr "Parámetros das pantallas"

#: core/startcountsource.cpp:31
msgctxt "KUserFeedback::StartCountSource|"
msgid "How often the application has been started."
msgstr "A frecuencia de inicio da aplicación."

#: core/startcountsource.cpp:46
msgctxt "KUserFeedback::StartCountSource|"
msgid "Launches count"
msgstr "Número de inicios"

#: core/usagetimesource.cpp:31
msgctxt "KUserFeedback::UsageTimeSource|"
msgid "The total amount of time the application has been used."
msgstr "A cantidade total de tempo de uso da aplicación."

#: core/usagetimesource.cpp:46
msgctxt "KUserFeedback::UsageTimeSource|"
msgid "Usage time"
msgstr "Tempo de uso"

#: widgets/auditlogbrowserdialog.cpp:28
msgctxt "KUserFeedback::AuditLogBrowserDialog|"
msgid "Delete Log"
msgstr "Eliminar o rexistro"

#: widgets/auditlogbrowserdialog.ui:14
msgctxt "KUserFeedback::AuditLogBrowserDialog|"
msgid "Submitted Data"
msgstr "Datos enviados"

#: widgets/auditlogbrowserdialog.ui:22
msgctxt "KUserFeedback::AuditLogBrowserDialog|"
msgid "Da&ta Submission:"
msgstr "Envío de da&tos:"

#: widgets/feedbackconfigdialog.cpp:32
msgctxt "KUserFeedback::FeedbackConfigDialog|"
msgid "Contribute!"
msgstr "Colaborar!"

#: widgets/feedbackconfigdialog.cpp:33
msgctxt "KUserFeedback::FeedbackConfigDialog|"
msgid "No, I do not want to contribute."
msgstr "Non quero colaborar."

#: widgets/feedbackconfigdialog.ui:14
msgctxt "KUserFeedback::FeedbackConfigDialog|"
msgid "Feedback Settings"
msgstr "Configuración de información"

#: widgets/feedbackconfigwidget.cpp:116
msgctxt "KUserFeedback::FeedbackConfigWidget|"
msgid "No data has been sent at this point."
msgstr "De momento non se enviaron datos."

#: widgets/feedbackconfigwidget.ui:17
msgctxt "KUserFeedback::FeedbackConfigWidget|"
msgid ""
"You can help the development of this software by contributing information on "
"how you use it, so the developers can focus on things that matter to you. "
"Contributing this information is optional and entirely anonymous. We never "
"collect your personal data, files you use, websites you visit, or "
"information that could identify you."
msgstr ""
"Pode colaborar no desenvolvemento deste software achegando información de "
"uso, para que as persoas desenvolvedoras se centren no que usa. Estas "
"achegas son opcionais e completamente anónimas. Non recollemos datos "
"persoais, ficheiros, visitas a sitios web, nin información que poida usarse "
"para a súa identificación."

#: widgets/feedbackconfigwidget.ui:33
msgctxt "KUserFeedback::FeedbackConfigWidget|"
msgid "Contribute Statistics"
msgstr "Enviar estatísticas"

#: widgets/feedbackconfigwidget.ui:107
msgctxt "KUserFeedback::FeedbackConfigWidget|"
msgid "TextLabel"
msgstr "Etiqueta de texto"

#: widgets/feedbackconfigwidget.ui:124
msgctxt "KUserFeedback::FeedbackConfigWidget|"
msgid "Show the raw data that is going to be shared."
msgstr "Amosar os datos en bruto que se van compartir."

#: widgets/feedbackconfigwidget.ui:137
msgctxt "KUserFeedback::FeedbackConfigWidget|"
msgid "<a href=\"auditLog\">View previously submitted data...</a>"
msgstr "<a href=\"auditLog\">Ver datos enviados previamente…</a>"

#: widgets/feedbackconfigwidget.ui:150
msgctxt "KUserFeedback::FeedbackConfigWidget|"
msgid "Participate in Surveys"
msgstr "Participar en enquisas"

#: widgets/notificationpopup.cpp:67
msgctxt "KUserFeedback::NotificationPopup|"
msgid "Help us make this application better!"
msgstr "Axúdenos a mellorar a aplicación!"

#: widgets/notificationpopup.cpp:68
msgctxt "KUserFeedback::NotificationPopup|"
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""
"Pode compartir estatísticas e participar en enquisas para axudarnos a "
"mellorar a aplicación."

#: widgets/notificationpopup.cpp:70
#, qt-format
msgctxt "KUserFeedback::NotificationPopup|"
msgid "Help us make %1 better!"
msgstr "Axúdenos a mellorar %1!"

#: widgets/notificationpopup.cpp:71
#, qt-format
msgctxt "KUserFeedback::NotificationPopup|"
msgid ""
"You can help us improving %1 by sharing statistics and participate in "
"surveys."
msgstr ""
"Pode compartir estatísticas e participar en enquisas para axudarnos a "
"mellorar %1."

#: widgets/notificationpopup.cpp:73
msgctxt "KUserFeedback::NotificationPopup|"
msgid "Contribute..."
msgstr "Colaborar…"

#: widgets/notificationpopup.cpp:84
msgctxt "KUserFeedback::NotificationPopup|"
msgid "We are looking for your feedback!"
msgstr "Queremos saber a túa opinión!"

#: widgets/notificationpopup.cpp:86
msgctxt "KUserFeedback::NotificationPopup|"
msgid ""
"We would like a few minutes of your time to provide feedback about this "
"application in a survey."
msgstr ""
"Gustaríanos roubarlle uns minutos do seu tempo para que forneza a súa "
"opinión sobre a aplicación nunha enquisa."

#: widgets/notificationpopup.cpp:88
#, qt-format
msgctxt "KUserFeedback::NotificationPopup|"
msgid ""
"We would like a few minutes of your time to provide feedback about %1 in a "
"survey."
msgstr ""
"Se ten uns minutos gustaríanos que completase unha enquisa para saber a súa "
"opinión sobre %1."

#: widgets/notificationpopup.cpp:89
msgctxt "KUserFeedback::NotificationPopup|"
msgid "Participate"
msgstr "Participar"

#: widgets/styleinfosource.cpp:23
msgctxt "KUserFeedback::StyleInfoSource|"
msgid ""
"The widget style used by the application, and information about the used "
"color scheme."
msgstr ""
"O estilo de trebellos que usa a aplicación, e información sobre a cor de "
"esquema que usa."

#: widgets/styleinfosource.cpp:37
msgctxt "KUserFeedback::StyleInfoSource|"
msgid "Application style"
msgstr "Estilo da aplicación"

#~ msgctxt "KUserFeedback::AuditLogBrowserDialog|"
#~ msgid "Dialog"
#~ msgstr "Diálogo"

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid "Basic usage statistics"
#~ msgstr "Estatísticas básicas de uso"

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid "Detailed usage statistics"
#~ msgstr "Estatísticas detalladas de uso"

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid ""
#~ "We make this application for you. You can help us improve it by "
#~ "contributing information on how you use it. This allows us to make sure "
#~ "we focus on things that matter to you.\n"
#~ "Contributing statistics is of course entirely anonymous, will not use any "
#~ "kind of unique identifier and will not cover any data you process with "
#~ "this application."
#~ msgstr ""
#~ "Facemos esta aplicación para vostede. Pode axudarnos a melloralo enviando "
#~ "información sobre como o usa. Isto permítenos asegurarnos de que nos "
#~ "centramos nas cousas que lle importan.\n"
#~ "As estatísticas enviadas son, por suposto, completamente anónimas, non "
#~ "usarán ningún tipo de identificador único e non conterán ningún dato que "
#~ "vostede procese coa aplicación."

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid ""
#~ "Share basic system information. No unique identification is included, nor "
#~ "data processed with the application."
#~ msgstr ""
#~ "Compartir información básica do sistema. Non se inclúe ningún "
#~ "identificador único, nin datos procesados coa aplicación."

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid ""
#~ "Share basic system information and basic statistics on how often you use "
#~ "the application. No unique identification is included, nor data processed "
#~ "with the application."
#~ msgstr ""
#~ "Compartir información básica do sistema e estatísticas básicas sobre a "
#~ "frecuencia coa que usa a aplicación. Non se inclúe ningún identificador "
#~ "único, nin datos procesados coa aplicación."

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid ""
#~ "Share detailed system information and statistics on how often individual "
#~ "features of the application are used. No unique identification is "
#~ "included, nor data processed with the application."
#~ msgstr ""
#~ "Compartir información detallada do sistema e estatísticas sobre a "
#~ "frecuencia coa que usa funcionalidades concretas da aplicación. Non se "
#~ "inclúe ningún identificador único, nin datos procesados coa aplicación."

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid ""
#~ "Share basic system information. No unique identification is included, nor "
#~ "data processed with %1."
#~ msgstr ""
#~ "Compartir información básica do sistema. Non se inclúe ningún "
#~ "identificador único, nin datos procesados con %1."

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid ""
#~ "We make this application for you. In order to ensure it actually does "
#~ "what you need it to do we would like to ask you about your use cases and "
#~ "your feedback, in the form of a web survey."
#~ msgstr ""
#~ "Facemos esta aplicación para vostede. Para asegurarse de que fai o que "
#~ "vostede necesita gustaríanos preguntarlle polos seus casos de uso e a súa "
#~ "opinión, a través dunha enquisa web."

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid ""
#~ "I will participate in web surveys whenever one is available. Surveys can "
#~ "of course be deferred or skipped."
#~ msgstr ""
#~ "Participarei en enquisas web en canto estean dispoñíbeis. Pero poderei "
#~ "atrasalas ou saltalas."

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid ""
#~ "We make %1 for you. In order to ensure it actually does what you need it "
#~ "to do we would like to ask you about your use cases and your feedback, in "
#~ "the form of a web survey."
#~ msgstr ""
#~ "Facemos %1 para vostede. Para asegurarse de que fai o que vostede "
#~ "necesita gustaríanos preguntarlle polos seus casos de uso e a súa "
#~ "opinión, a través dunha enquisa web."

#~ msgctxt "KUserFeedback::FeedbackConfigUiController|"
#~ msgid "No telemetry"
#~ msgstr "Non hai telemetría"
